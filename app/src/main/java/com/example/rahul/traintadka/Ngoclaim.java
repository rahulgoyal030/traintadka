package com.example.rahul.traintadka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

public class Ngoclaim extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ngoclaim);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String fcm = extras.getString("fcm");
        String username = extras.getString("username");
        JSONObject obj = new JSONObject();
        try {
            obj.put("username",username);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            obj.put("fcm",fcm);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("jsonobject",obj.toString());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://1bb64807.ngrok.io/backend/fcm";
        JsonRequest jsonRequest = new JsonObjectRequest(url,obj, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                // Display the first 500 characters of the response string.
                Log.d("res1","Response is: "+ response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("res2","Response is: "+ "error");
            }
        });
// Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }
}
