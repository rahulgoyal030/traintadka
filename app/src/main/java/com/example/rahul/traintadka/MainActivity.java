package com.example.rahul.traintadka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import static android.provider.AlarmClock.EXTRA_MESSAGE;
import static java.sql.Types.NULL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = new Bundle();
        if(getIntent().getExtras()!=null)
        {
            Log.d("gift","intentwork");
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                Log.d("result", "Key: " + key + " Value: " + value);
                extras.putString(key,value);


            }
        }
        String s = SaveSharedPreference.getUserName(MainActivity.this);
        if(s.length() == 0)
        {
            // call Login Activity
            setContentView(R.layout.activity_main);
        }
        else
        {
            // Stay at the current activity.
            if(s=="vendor")
            {
                Intent intent = new Intent(this , Mainvendor.class);
                startActivity(intent);
            }
            else
            {
                Intent intent = new Intent(this , Mainngo.class);
                intent.putExtras(extras);
                startActivity(intent);
            }
        }


    }
    public void call_vendor(View view)
    {
        Intent intent = new Intent(this,Vendor.class);
        intent.putExtra(EXTRA_MESSAGE,"Hello");
        startActivity(intent);
    }
    public void call_ngo(View view)
    {
        Intent intent = new Intent(this,Ngo.class);
        intent.putExtra(EXTRA_MESSAGE,"Hello");
        startActivity(intent);
    }
}
