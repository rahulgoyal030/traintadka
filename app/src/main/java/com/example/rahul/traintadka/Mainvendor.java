package com.example.rahul.traintadka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class Mainvendor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainvendor);
    }
    public void main_vendor(View view) throws JSONException {
        JSONObject obj = new JSONObject();

        EditText edit1 = (EditText)findViewById(R.id.train);
        String train = edit1.getText().toString();
        EditText edit2 = (EditText)findViewById(R.id.veg);
        String veg = edit2.getText().toString();
        EditText edit3 = (EditText)findViewById(R.id.nonveg);
        String nonveg = edit3.getText().toString();
        obj.put("train",train);
        obj.put("veg",veg);
        obj.put("nonveg",nonveg);
        Log.d("jsonobject",obj.toString());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://1bb64807.ngrok.io/backend/food-details";
        final Intent intent = new Intent(this , ngo_selected.class);
// Request a string response from the provided URL.
        JsonRequest jsonRequest = new JsonObjectRequest(url,obj, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                // Display the first 500 characters of the response string.
                Log.d("res1","Response is: "+ response.toString());

                intent.putExtra(EXTRA_MESSAGE,"Hello");
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("res2","Response is: "+ "error");
                intent.putExtra(EXTRA_MESSAGE,"Hello");
                startActivity(intent);
            }
        });
// Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }
    }

