package com.example.rahul.traintadka;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

public class Mainngo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainngo);
        Intent intent = getIntent();
        if(intent.getExtras()!=null) {
            String veg = intent.getStringExtra("veg");
            String nonveg = intent.getStringExtra("nonveg");
            String station = intent.getStringExtra("station");
            String train = intent.getStringExtra("train");
            Log.d("baba", veg);
            TextView tv = (TextView) findViewById(R.id.station);
            tv.append(": "+station);
            TextView tv1 = (TextView) findViewById(R.id.train);
            tv1.append(": "+train);
            TextView tv2 = (TextView) findViewById(R.id.veg);
            tv2.append(": "+veg);
            TextView tv3 = (TextView) findViewById(R.id.nonveg);
            tv3.append(": "+nonveg);
        }
    }
    public void finish(View view)
    {
        Intent intent = new Intent(this , finish.class);
        startActivity(intent);
    }
}