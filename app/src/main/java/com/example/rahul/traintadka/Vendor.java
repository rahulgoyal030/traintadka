package com.example.rahul.traintadka;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

import org.json.JSONException;
import org.json.JSONObject;


public class Vendor extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);
    }
    public void main_vendor(View view) throws JSONException {
        JSONObject obj = new JSONObject();

        EditText edit1 = (EditText)findViewById(R.id.vendor_id);
        final String id = edit1.getText().toString();
        EditText edit2 = (EditText)findViewById(R.id.vendor_pwd);
        String pwd = edit2.getText().toString();
        final Context ctx = this;
        obj.put("username",id);
        obj.put("password",pwd);
        Log.d("jsonobject",obj.toString());
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://1bb64807.ngrok.io/backend/login";
        final Intent intent = new Intent(this , Mainvendor.class);
// Request a string response from the provided URL.
        JsonRequest jsonRequest = new JsonObjectRequest(url,obj, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.
                        Log.d("res1","Response is: "+ response.toString());
                        //SaveSharedPreference.setUserName(ctx,id,"vendor");
                        intent.putExtra(EXTRA_MESSAGE,"Hello");
                        startActivity(intent);
            }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("res2","Response is: "+ error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(jsonRequest);
   }
}
