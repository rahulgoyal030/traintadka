package com.example.rahul.traintadka;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Lovekesh Pahuja on 5/7/2017.
 */

public class SaveSharedPreference
{
    static final String PREF_USER_NAME= "username";
    static final String TYPE = "type";
    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName,String type)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.putString(TYPE,type);
        editor.commit();
    }

    public static String getUserName(Context ctx)
    {
        String s =  getSharedPreferences(ctx).getString(TYPE, "");
        return s;
    }
    public  static String getUser(Context ctx)
    {
        String s =  getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
        return s;
    }
}